# :notebook: Negation and Sentiment Detection on Mexican Spanish Tweets: The T-MexNeg Corpus

In Spanish, there are three basics levels of negation: lexical, morphological, and syntactic. This corpus addreesses only the syntactic negation. Negative sentences express false states or the nonexistence of the action that is in the sentence and they might also change sentiment within lexical alignments in a text. Syntax negation is a syntax operator word that affects the whole sentence or a section of it. This syntax operator is called negation cue. They can be adverbs, prepositions, indefinite pronouns, and conjunctions. Usually, in Spanish, negation cues precede the verb, but they can also appear postponed.

The section affected by the negation cue is called scope. The words that are specifically reached by it, which can be verbs, nouns,  or phrases, are referred to as event. Therefore, the basic requirements to create a negative sentence are the negation cue, the scope, and the event.

## :page_facing_up: Corpus Description

The T-MexNeg corpus of Tweets written in Mexican Spanish. It consists of 13,704 Tweets,  of which 4895 contain negation structures.

The corpus is the result of an analysis of sentiment and negation statements embedded in the language employed on social media. This repository includes annotation guidelines along with the corpus, manually annotated with labels of sentiment, negation cue, scope, and, event.

Twitter was used as the innitial source of the corpus; the tweets are a random subset of a set collected from Mexican users from September 2017 to April 2019.

## :paperclip: Tags Description

Each entry in the corpus consists of a tweet with two components: the content, and the sentiment tag.

Within the content, the annotation identifies three main negation components: Negation Cue, Event, and Scope. It also differentiate among three types of negation cues: Simple Negation (**neg_exp**), Related Negation (**neg_rel**), and False Negation (**no_neg**). 

- **neg_exp** : It refers to the negation cues that are not linked to other negation cues \ref{1c}. Thus, the Scope and the Event are only directly related to this negation.
- **neg_rel** : This label is used for negation cues that are linked to other negation cues in the sentence and are dependent on them. The related negation does not have an event or scope and it is part of the scope of the main negation.
- **no_neg** : This tag is used with negation cues that do not negate anything at a semantic level, as well as with some abbreviations and idiomatic phrases and discursive markers.

- **event** : The Event labels the word or words that are specifically negated.
- **scope** : This tag corresponds to all words that are affected by the negation.

The general structure of an entry in the corpus would present the tags as follows:


```
<tweet>
    <polarity>
        'NEGATIVE/POSITIVE/NEUTRAL'
    </polarity>
    <content>
        <neg_structure>
            <scope>
                <negexp class='simple/related/no_neg'>
                </negexp>
                <event>
                </event>
            </scope>
        </neg_structure>
    </content>
</tweet>
```

## :pencil: Citing

If you use the corpus please use the following BibTeX:

```
@Article{app11093880,
AUTHOR = {Bel-Enguix, Gemma and Gómez-Adorno, Helena and Pimentel, Alejandro and Ojeda-Trueba, Sergio-Luis and Aguilar-Vizuet, Brian},
TITLE = {Negation Detection on Mexican Spanish Tweets: The T-MexNeg Corpus},
JOURNAL = {Applied Sciences},
VOLUME = {11},
YEAR = {2021},
NUMBER = {9},
ARTICLE-NUMBER = {3880},
URL = {https://www.mdpi.com/2076-3417/11/9/3880},
ISSN = {2076-3417},
DOI = {10.3390/app11093880}
}
```

## Aknowledgments

This resource was funded by CONACyT project CB A1-S-27780, DGAPA-UNAM PAPIIT grants number TA400121 and TA100520.
